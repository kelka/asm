/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_err.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: k6 <klo.elka@gmail.com>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/07 00:47:49 by k6                #+#    #+#             */
/*   Updated: 2014/06/07 06:43:28 by k6               ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <asm.h>

void					write_err(int errn)
{
	static char			err_tab[][MAX_E_LEN] = {
		{": problem with the first param.\n"},
		{": problem with the second param.\n"},
		{": problem with the third param.\n"},
		{": bad program name.\n"},
		{": bad header comment.\n"},
		{": bad instruction.\n"},
		{": wrong number of parameters.\n"},
		{": you just wrote nonsense.\n"},
		{": malloc fail.\n"},
		{": problem at reading input file.\n"},
		{": problem at reading output file.\n"},
		{": cannot open output file for writing.\n"},
	};
	ft_putstr(err_tab[errn * -1 - 1]);
}

void					sys_err(int errn)
{
	ft_putstr("\033[31merror\033[0m");
	write_err(errn);
}

void					parse_err(int line_nb, int errn, char **line)
{
	ft_putstr("\033[31m[\033[0m");
	ft_putnbr(line_nb);
	ft_putstr("\033[31m]\033[0m");
	ft_putstr("\033[31m syntax error\033[0m");
	write_err(errn);
	ft_putstr("\t=>");
	ft_putendl(*line);
	free(*line);
	*line = NULL;
}
