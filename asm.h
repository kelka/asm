/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: k6 <klo.elka@gmail.com>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/20 02:54:14 by k6                #+#    #+#             */
/*   Updated: 2014/06/10 15:43:27 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ASM_H
# define ASM_H

# include <op.h>
# include <libft.h>
# include <fcntl.h>
# include <get_next_line.h>
//PRINTF
# include <stdio.h>
//POW
# include <math.h>

# define __DEBUG_ASM_PARSER__	1

# define M_ISSET(x, mask)	(mask & x ? 1 : 0)
# define M_SET(x, mask, n)	(mask += x << ((MAX_ARGS_NUMBER - (n + 1)) * 2))
# define M_GET(mask, n)		(mask >> ((MAX_ARGS_NUMBER -(n + 1)) * 2) & (3))

# define EWRITE(fd, ptr, len, tlen)	(write(fd, (unsigned char *)swap_endian(ptr, len), len) && (*tlen += len))
# define WRITE(fd, ptr, len, tlen)	(write(fd, (unsigned char *)ptr, len) && (*tlen += len))

/*
** Number of bytes written per arguments
*/
# define REG_LEN	1
# define DIR_LEN	sizeof(int)
# define IND_LEN	DIR_LEN

/*
** Instruction separator char in .cor
*/
# define EOI	"\n"

/*
** PARSING
** =============================================================================
*/

/*
** At this point, endianness is based on OS one. Need conversion to big endian.
** (eventually).
*/

typedef struct s_lab_hdl	t_lab_hdl;
typedef char				(*ftp_args[3])(char **, char **, t_lab_hdl *);
typedef struct s_labels		t_labels;
typedef struct s_instruct	t_instruct;
typedef struct s_at			t_at;

struct			s_instruct
{
	char		*label;
	char		opcode;
	unsigned char		args_type;
	char		*args[MAX_ARGS_NUMBER];
	t_instruct	*next;
};

struct			s_labels
{
	char		*name;
	size_t		nam_len;
	int			offset;
	t_at		*at;
	t_labels	*next;
};

struct			s_at
{
	int			line;
	int			n_arg;
	char		*name;
	size_t		offset;
	char		len;
	t_at		*next;
};

struct			s_lab_hdl
{
	t_at		*ats;
	t_labels	*labels;
	int			cur_line;
	int			n_arg;
	size_t		cur_len;
};

char			asm_parse(char **argv, t_header **s_header, t_instruct **s_instructs, t_lab_hdl *s_lab_hdl);

/*** add_instruct: Malloc and return a new s_instruct (or NULL if Malloc fail)
** then put it a the end of the list pointed to by add_instruct and update
** *begin. free_instruct free all this list starting at *begin.
*/
t_instruct		*add_instruct(t_instruct **s_begin);
void			free_instruct(t_instruct **begin);

/*
** parse an instruction and store it to s_instruct:
** get it's opcode(add_op), it's label if any(add_label) and the arguments
** (add_args), using the "new_smthg" functions below. Those functions returns 0
** on a parsing error or 1 if successfull. opcode, type of args.. are found in
** t_op (op.c / op.h) param.
*/

char			get_instruct(char **line, t_instruct **s_instruct,
		t_op *s_op, t_lab_hdl *s_lab_hdl);
char			get_arg_type(char **p_line, t_op s_op, int n_arg);

char			add_label(char **line, char **p_line,
	t_instruct *s_instruct, size_t *op_namlen, t_lab_hdl *s_lab_hdl);
char			add_op(char **p_line, t_instruct *s_instruct,
	size_t op_namlen, t_op *s_op, t_lab_hdl *s_lab_hdl);
char			add_args(t_op s_op, t_instruct *s_instruct, char **p_line, t_lab_hdl *s_lab_hdl);

char			new_ind(char **arg, char **p_line, t_lab_hdl *s_lab_hdl);
char			new_dir(char **arg, char **p_line, t_lab_hdl *s_lab_hdl);
char			new_reg(char **arg, char **p_line, t_lab_hdl *s_lab_hdl);

t_labels		*new_label(t_labels **s_labels);
t_labels		*get_label(char *name, t_labels *s_labels);

/*
** WRITE .COR FILE
** =============================================================================
*/

char			write_cor(char *pathname, t_instruct *s_instruct,
	t_header *s_header, t_lab_hdl *s_lab_hdl);
void			write_instruct(int fd, t_instruct *s_instruct,
		unsigned int *prog_size, t_lab_hdl *s_lab_hdl);
void			write_head(int fd, t_header *s_header);

t_at			*new_label_at(t_at **at, char *label_name, int line);

/*
** ERRORS
** =============================================================================
*/

void			parse_err(int line_nb, int errn, char **line);
void			sys_err(int errn);

# define MAX_E_LEN	100

# define E_PARM1	-1
# define E_PARM2	-2
# define E_PARM3	-3
# define E_PNAME	-4
# define E_COMNT	-5
# define E_INSTR	-6
# define E_WRGPN	-7
# define E_NONSE	-8

# define E_WLABL	-9

# define SYS_ERR	-10

# define E_MALLC	-10
# define E_OPNIF	-11
# define E_OPNOF	-12
# define E_READF	-13

/*
** UTILS
** =============================================================================
*/

void			*swap_endian(void *src, int len);
char			is_big_endian(void);

#endif
