/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap_endian.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: k6 <klo.elka@gmail.com>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/23 06:58:52 by k6                #+#    #+#             */
/*   Updated: 2014/06/01 05:56:46 by k6               ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <libft.h>

char				is_big_endian(void)
{
	int				i;

	i = 1;
	if (*(char *)(&i))
		return (0);
	return (1);
}

void				*swap_endian(void *src, int len)
{
	unsigned char	swapped[len];
	int				n;
	int				save_len;

	save_len = --len;
	n = 0;
	while (len > -1)
	{
		*(swapped + n) = (*(unsigned char *)(src + len));
		--len;
		++n;
	}
	while (save_len > -1)
	{
		*(unsigned char *)(src + save_len) = *(swapped + save_len);
		--save_len;
	}
	return (src);
}
