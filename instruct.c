/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instruct.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: k6 <klo.elka@gmail.com>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/20 02:57:42 by k6                #+#    #+#             */
/*   Updated: 2014/06/07 03:44:05 by k6               ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <asm.h>

t_instruct		*add_instruct(t_instruct **begin)
{
	t_instruct	*tmp;

	if (!*begin && !(*begin = ft_memalloc(sizeof(**begin))))
		return (NULL);
	else
	{
		tmp = *begin;
		while (tmp->next)
			tmp = tmp->next;
		if (!(tmp->next = ft_memalloc(sizeof(*tmp->next))))
			return (NULL);
		tmp->label = NULL;
		return (tmp);
	}
	return (*begin);
}

void			free_instruct(t_instruct **begin)
{
	t_instruct	*tmp;
	int			n;

	n = -1;
	while (*begin)
	{
		tmp = *begin;
		*begin = (*begin)->next;
		if (tmp->label)
			free(tmp->label);
		while (++n < MAX_ARGS_NUMBER)
			free(tmp->args[n]);
		free(tmp);
	}
	*begin = NULL;
}
