/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_instruct_arg.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: k6 <klo.elka@gmail.com>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/23 04:35:52 by k6                #+#    #+#             */
/*   Updated: 2014/06/10 15:51:01 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <asm.h>

static char			atoi_arg(char **arg, char **p_line, int len)
{
	size_t			val_len;
	int				end;
	int				val;

	val_len = 0;
	end = 0;
	if (**p_line == '-' && ++val_len)
		++*p_line;
	while (**p_line && ft_isdigit(**p_line) && ++val_len && ++*p_line)
		;
	if ((**p_line == SEPARATOR_CHAR) || (!**p_line && (end = 1)) || ft_isblank(**p_line))
	{
		**p_line = '\0';
		*arg = ft_memalloc(len);
		val = ft_atoi(*p_line - val_len);
		ft_memcpy(*arg, &val, len);
		if (!end)
			++*p_line;
		while (ft_isblank(**p_line))
			++*p_line;
		if ((**p_line == EOI_CHAR) && !(**p_line = '\0'))
			end = 1;
		return (1);
	}
	return (0);
}

char				str_arg(char **arg, char **p_line, char *chars)
{
	size_t			val_len;
	int				end;

	val_len = 0;
	++*p_line;
	++val_len;
	end = 0;
	while (**p_line && !ft_isblank(**p_line) && ft_strchr(chars, **p_line) && ++val_len
			&& ++*p_line)
		;
	if (((**p_line == SEPARATOR_CHAR) || (!**p_line && (end = 1)) || ft_isblank(**p_line)) && !(**p_line = '\0'))
	{
		*arg = ft_memalloc(val_len + 1);
		*(*arg + val_len) = '\0';
		ft_strncpy(*arg, *p_line - val_len, val_len);
		if (!end)
			++*p_line;
		while (ft_isblank(**p_line))
			++*p_line;
		if ((**p_line == EOI_CHAR) && !(**p_line = '\0'))
			end = 1;
		return (1);
	}
	else
		return (0);
}

static char			new_dir_label(char **arg, char **p_line, t_lab_hdl *s_lab_hdl)
{
	char			ret;
	t_at			*s_at;

	ret = str_arg(arg, p_line, LABEL_CHARS);
	if (!(s_at = new_label_at(&s_lab_hdl->ats, *arg, s_lab_hdl->cur_line)))
		return (E_MALLC);
	s_at->n_arg = s_lab_hdl->n_arg;
	s_at->len = s_lab_hdl->cur_len;
	return (ret);
}

char				new_dir(char **arg, char **p_line, t_lab_hdl *s_lab_hdl)
{
	int				ret;

	if (**p_line == ':')
	{
		ret = new_dir_label(arg, p_line, s_lab_hdl);
		return (ret);
	}
	else
		return (atoi_arg(arg, p_line, DIR_LEN));
}

char				new_ind(char **arg, char **p_line, t_lab_hdl *s_lab_hdl)
{
	return (atoi_arg(arg, p_line, IND_LEN));
}

char				new_reg(char **arg, char **p_line, t_lab_hdl *s_lab_hdl)
{
	char			ret;
	
	ret = atoi_arg(arg, p_line, REG_LEN);
	if (ret && (**arg > 0) && (**arg <= REG_NUMBER))
		return (ret);
	return (0);
}
