# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: k6 <klo.elka@gmail.com>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/06/01 05:45:09 by k6                #+#    #+#              #
#    Updated: 2014/06/10 03:45:41 by ccano            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = asm
SRC = add_instruct_param.c get_instruct.c instruct.c new_instruct_arg.c \
	  get_next_line.c init_counter.c write_header.c label.c\
	  op.c swap_endian.c asm.c write_cor.c main.c asm_err.c
OBJ = $(SRC:.c=.o)
CFLAGS += -Wall -Wextra -ggdb -O3
LIBFT = $(HOME)/lib/libft
LDFLAGS = -L$(LIBFT) -lft
INC = -I. -I$(LIBFT) -IgetNextLine

all: $(NAME)

$(NAME):
	gcc -o$(NAME) $(CFLAGS) $(INC) $(SRC) -L$(LIBFT) -lft -lm

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all
