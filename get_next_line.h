/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lverniss <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/06 23:36:21 by lverniss          #+#    #+#             */
/*   Updated: 2014/05/21 04:11:24 by k6               ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef		GET_NEXT_LINE_H
# define	GET_NEXT_LINE_H
// Include
#include	<stdlib.h>
#include	<sys/types.h>
#include	<unistd.h>
#include	<sys/uio.h>
// Define
#define		BUFF_SIZE	10

typedef struct	s_get_line
{
  char	buff[BUFF_SIZE];
  int		counter;
  struct	s_get_line	*next;
}				t_get_line;
// Get next line proto
int			get_next_line(int const fd, char **line);
void		init_counter(int *counter, int const *src, int size);

#endif // GET_NEXT_LINE_H
