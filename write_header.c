/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_header.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 00:09:14 by ccano             #+#    #+#             */
/*   Updated: 2014/06/11 14:26:35 by k6               ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <asm.h>

static void	norm_write(int fd, t_header *s_header, unsigned int *prog_size)
{
	WRITE(fd, &s_header->magic, sizeof(s_header->magic), prog_size);
	*prog_size += (sizeof(s_header->prog_size));
	EWRITE(fd, s_header->prog_name, PROG_NAME_LENGTH, prog_size);
	EWRITE(fd, s_header->comment, COMMENT_LENGTH, prog_size);
	write(1, "\tName: ", 7);
	write(1, s_header->prog_name, PROG_NAME_LENGTH);
	write(1, "\n", 1);
	write(1, "\t\t", 2);
	write(1, s_header->comment, COMMENT_LENGTH);
	write(1, "\n", 1);
}

static void swap_write(int fd, t_header *s_header, unsigned int *prog_size)
{
	EWRITE(fd, &s_header->magic, sizeof(s_header->magic), prog_size);
	*prog_size += (sizeof(s_header->prog_size));
	WRITE(fd, s_header->prog_name, PROG_NAME_LENGTH, prog_size);
	write(1, "\tName: ", 7);
	write(1, s_header->prog_name, PROG_NAME_LENGTH);
	write(1, "\n", 1);
	WRITE(fd, &s_header->prog_size, sizeof(s_header->prog_size), prog_size);
	WRITE(fd, &s_header->comment, COMMENT_LENGTH, prog_size);
	write(1, "\t\t", 2);
	write(1, s_header->comment, COMMENT_LENGTH);
	write(1, "\n", 1);
}

void		write_head(int fd, t_header *s_header)
{
	s_header->magic = COREWAR_EXEC_MAGIC;
	s_header->prog_size = 0;
	if (is_big_endian())
		norm_write(fd, s_header, &s_header->prog_size);
	else
		swap_write(fd, s_header, &s_header->prog_size);
}
