/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_instruct_param.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: k6 <klo.elka@gmail.com>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/23 04:26:01 by k6                #+#    #+#             */
/*   Updated: 2014/06/10 16:19:19 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <asm.h>

static void			init_ftp_arg(ftp_args *ftp_arg)
{
	*(*ftp_arg + REG_CODE - 1) = &new_reg;
	*(*ftp_arg + DIR_CODE - 1) = &new_dir;
	*(*ftp_arg + IND_CODE - 1) = &new_ind;
}

char				add_args(t_op s_op, t_instruct *s_instruct, char **p_line, t_lab_hdl *s_line_hdl)
{
	char			type;
	ftp_args		ftp_arg;

	s_line_hdl->n_arg = 0;
	init_ftp_arg(&ftp_arg);
#if __DEBUG_ASM_PARSER__
	printf("\t\targs:\n");
#endif
	while (**p_line && s_line_hdl->n_arg < s_op.nb_args)
	{
		while (ft_isblank(**p_line))
			++*p_line;
		if ((type = get_arg_type(p_line, s_op, s_line_hdl->n_arg)) < 0)
			return ((s_line_hdl->n_arg + 1) * -1);
		M_SET(type, s_instruct->args_type, s_line_hdl->n_arg);
		s_line_hdl->cur_len = s_op.has_idx ? IND_SIZE : DIR_LEN;
		if ((type = ftp_arg[(int)type - 1](&s_instruct->args[s_line_hdl->n_arg], p_line, s_line_hdl))
				< 1)
			return ((s_line_hdl->n_arg + 1) * -1);
		++s_line_hdl->n_arg;
	}
	if (s_line_hdl->n_arg < s_op.nb_args || **p_line)
		return (E_WRGPN);
#if __DEBUG_ASM_PARSER__
		printf("\t\targs type: %2x\n", s_instruct->args_type);
#endif
	return (1);
}

char				add_op(char **p_line, t_instruct *s_instruct,
		size_t op_namlen, t_op *s_op, t_lab_hdl *s_lab_hdl)
{
	int				n;
	int				cmp;

	n = 0;
	while (ft_strlen(s_op[n].name) != op_namlen)
		++n;
	while ((n < NB_OP) && (cmp = strncmp(*p_line - op_namlen, s_op[n].name,
					op_namlen)))
	{
		++n;
		while ((n < NB_OP) && ft_strlen(s_op[n].name) != op_namlen && ++n)
			cmp = 1;
	}
	if (cmp)
		return (E_INSTR);
	else
	{
		s_instruct->opcode = s_op[n].opcode;
#if __DEBUG_ASM_PARSER__
		printf("\t\top name:%s\n\t\topcode: %d\n", s_op[n].name,
				s_instruct->opcode);
#endif
	}
	return (add_args(s_op[n], s_instruct, p_line, s_lab_hdl));
}

char				add_label(char **line, char **p_line,
		t_instruct *s_instruct, size_t *op_namlen, t_lab_hdl *s_lab_hdl)
{
	t_labels		*label;

	s_instruct->label = ft_memalloc(*op_namlen + 2);
	ft_memcpy(s_instruct->label + 1, *line, *op_namlen);
	*s_instruct->label = LABEL_CHAR;
#if __DEBUG_ASM_PARSER__
	printf("\t\tlabel: %s\n", s_instruct->label);
#endif
	label = new_label(&s_lab_hdl->labels);
	label->name = s_instruct->label;
	label->nam_len = ft_strlen(s_instruct->label);
	++*p_line;
	*op_namlen = 0;
	while (**p_line && ft_isblank(**p_line))
		++*p_line;
	while (ft_isalpha(**p_line) && ++*p_line)
		++*op_namlen;
	return (1);
}
