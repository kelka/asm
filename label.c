/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   label.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/10 03:19:06 by ccano             #+#    #+#             */
/*   Updated: 2014/06/10 14:58:52 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <asm.h>

t_labels				*new_label(t_labels **s_labels)
{
	t_labels			*p_labels;
	t_labels			*new;
	
	new = ft_memalloc(sizeof(*new));
	p_labels = *s_labels;
	if (p_labels && new)
	{
		while (p_labels->next)
			p_labels = p_labels->next;
		p_labels->next = new;
	}
	else if (new)
		*s_labels = new;
	return (new);
}

t_labels				*get_label(char *name, t_labels *s_labels)
{
	size_t				nam_len;
	int					cmp;

	nam_len = ft_strlen(name);
	while (s_labels)
	{
		if (nam_len == s_labels->nam_len && !(cmp = ft_strncmp(name,
			s_labels->name, nam_len)))
			break;
		s_labels = s_labels->next;
	}
	return (s_labels);
}

t_at					*new_label_at(t_at **s_at, char *name, int line)
{
	t_at				*p_at;
	t_at				*new;

	new = NULL;
	if ((new = ft_memalloc(sizeof(*new))))
	{
		p_at = *s_at;
		if (p_at)
		{
			while (p_at->next)
				p_at = p_at->next;
			p_at->next = new;
		}
		else
			*s_at = new;
		new->line = line;
		new->name = name;
	}
	return (new);
}
