void		init_counter(int *counter, int const *src, int size)
{
   int		n;

   n = -1;
   while (++n < size)
	  *(counter + n) = src[n];
}
