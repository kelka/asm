/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: k6 <klo.elka@gmail.com>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/20 02:28:25 by k6                #+#    #+#             */
/*   Updated: 2014/06/10 01:41:22 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OP_H
# define OP_H

/*les tailles sont en octets.
**On part du principe qu'un int fait 32 bits. Est-ce vrai chez vous ?
*/
# define NB_OP					16

# define IND_SIZE				2
# define REG_SIZE				sizeof(int)
# define DIR_SIZE				REG_SIZE

# define REG_CODE				1
# define DIR_CODE				2
# define IND_CODE				3

# define MAX_ARGS_NUMBER		4
# define MAX_PLAYERS			4
# define MEM_SIZE				(4*1024)
# define IDX_MOD				(MEM_SIZE / 8)
# define CHAMP_MAX_SIZE			(MEM_SIZE / 6)

# define COMMENT_CHAR			'#'
# define LABEL_CHAR				':'
# define DIRECT_CHAR			'%'
# define REG_CHAR				'r'
# define SEPARATOR_CHAR			','
# define EOI_CHAR				';'

# define LABEL_CHARS			"abcdefghijklmnopqrstuvwxyz_0123456789"

# define NAME_CMD_STRING		".name"
# define COMMENT_CMD_STRING		".comment"

# define REG_NUMBER				16

# define CYCLE_TO_DIE			1536
# define CYCLE_DELTA			50
# define NBR_LIVE				21
# define MAX_CHECKS				10

/*
 * **
 * */

typedef char	t_arg_type;

# define T_REG					1
# define T_DIR					2
# define T_IND					4
# define T_LAB					8
# define T_NAM					16
# define T_COM					32
# define T_SEP					64

/*
 * **
 * */

# define PROG_NAME_LENGTH		(132)
# define COMMENT_LENGTH			(2048)
# define COREWAR_EXEC_MAGIC		0xea83f3

typedef struct		s_header
{
	unsigned int	magic;
	char		prog_name[PROG_NAME_LENGTH + 1];
	unsigned int	prog_size;
	char		comment[COMMENT_LENGTH + 1];
}					t_header;

typedef struct		s_op
{
	char			*name;
	char			nb_args;
	char			args_type[MAX_ARGS_NUMBER];
	char			opcode;
	int				nb_clock;
	char			comment[COMMENT_LENGTH];
	char			carry;
	char			has_idx;
}					t_op;

t_op	*get_optab();

#endif
