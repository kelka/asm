/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lverniss <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/06 23:39:03 by lverniss          #+#    #+#             */
/*   Updated: 2013/12/18 14:11:45 by lverniss         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "get_next_line.h"

char					*convert_lst(t_get_line *start)
{
	const int			counter[4] = {1, 0, -1, 0};
	int					counters[4];
	char				*line;
	t_get_line			*tmp;

	tmp = start;
	init_counter((int *)(counters), (const int *)counter, 4);
	while (start->next)
	{
		counters[0] += start->counter;
		start = start->next;
	}
	while (++counters[2] < start->counter && start->buff[counters[2]] != '\n')
		counters[0]++;
	if (!(line = malloc(sizeof(*line) * counters[0])))
		return (0);
	while (tmp)
	{
		counters[1] = 0;
		while (counters[1] < tmp->counter && tmp->buff[counters[1]] != '\n')
			line[counters[3]++] = tmp->buff[counters[1]++];
		tmp = tmp->next;
	}
	line[counters[3]]= '\0';
	return (line);
}

int						read_next_line(int const fd, t_get_line *play)
{
	t_get_line			*tmp;
	int					counter;

	if (play->counter)
	{
		if (!(play->next = malloc(sizeof(*play->next))))
			return (0);
		play = play->next;
		play->next = 0;
	}
	tmp = play;
	while ((tmp->counter = read(fd, tmp->buff, BUFF_SIZE)) == BUFF_SIZE)
	{
		counter = -1;
		while (++counter < tmp->counter)
			if (tmp->buff[counter] == '\n')
				return (1);
		if (!(tmp->next = malloc(sizeof(*tmp))))
			return (0);
		tmp = tmp->next;
		tmp->next = 0;
	}
	return (1);
}

char					*test_struct(t_get_line *test)
{
	int					counter;

	counter = -1;
	while (++counter < test->counter)
	{
		if (test->buff[counter] == '\n')
		  return (convert_lst(test));
	}
	return (0);
}

void					free_lst(t_get_line *free_lst)
{
	t_get_line			*lst;
	t_get_line			*tmp;
	int					counters[2];

	counters[0] = -1;
	lst = free_lst->next;
	while (lst && lst->next)
	{
		tmp = lst;
		lst = lst->next;
		free(tmp);
	}
	if (lst == NULL)
		lst = free_lst;
	counters[1] = 0;
	while (++counters[0] < lst->counter && lst->buff[counters[0]] != '\n')
	  ;
	while (++counters[0] < lst->counter)
	  free_lst->buff[counters[1]++] = lst->buff[counters[0]];
	if (lst != free_lst)
	  free(lst);
	free_lst->counter = counters[1];
	free_lst->next = 0;
}

int						get_next_line(int const fd, char **line)
{
	static t_get_line	s_l = {{0}, 0, 0};

	if (s_l.counter)
	{
		if ((*line = test_struct(&s_l)))
		{
			free_lst(&s_l);
			return (1);
		}
	}
	if (!(read_next_line(fd, &s_l)))
		return (-1);
	if (s_l.counter == 0)
		return (0);
	if ((*line = convert_lst(&s_l)))
	{
		free_lst(&s_l);
		return (1);
	}
	return (-1);
}
