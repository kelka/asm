/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: k6 <klo.elka@gmail.com>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/21 03:50:05 by k6                #+#    #+#             */
/*   Updated: 2014/06/10 14:58:50 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <asm.h>

char				get_comment(void)
{
#if __DEBUG_ASM_PARSER__
	printf("\033[33m\tnew comment\n\033[0m");
#endif
	return (1);
}

char				get_prog_name(t_header *s_header, char *line, int len)
{
	int				spc;

	spc = 0;
	while (ft_isblank(*line) && (spc = 1))
		++line;
	if ((*s_header->prog_name) || !spc
			|| ((len = ft_strlen(line)) > PROG_NAME_LENGTH)
			||(*(line + len - 1) != '"') || (*line != '"'))
		return (E_PNAME);
	ft_memcpy(s_header->prog_name, line + 1, len - 2);
	*(s_header->prog_name + len) = '\0';
#if __DEBUG_ASM_PARSER__
	printf("\033[35mprog name:\033[0m %s\n", s_header->prog_name);
#endif
	return (1);
}

char				get_prog_comment(t_header *s_header, char *line, int len)
{
	int				spc;

	spc = 0;
	while (ft_isblank(*line) && (spc = 1))
		++line;
	if (*s_header->comment || !spc
			|| ((len = ft_strlen(line)) > COMMENT_LENGTH)
			||(*(line + len - 1) != '"') || (*line != '"'))
		return (E_COMNT);
	ft_memcpy(s_header->comment, line + 1, len - 2);
	*(s_header->comment + len) = '\0';
#if __DEBUG_ASM_PARSER__
	printf("\033[35mheader comment:\033[0m %s\n", s_header->comment);
#endif
	return (1);
}

char				get_header_info(t_header **s_header, char *line)
{
	int				len;
	int				ret;

	len = ft_strlen(line + 1);
	if (!*s_header && (!(*s_header = ft_memalloc(sizeof(**s_header)))))
		return (E_MALLC);
	if (len >= 7 && !ft_memcmp((line + 1), "comment", 7))
		return (get_prog_comment(*s_header, line + 8, len - 7));
	if (len >= 4 && !(ret = ft_memcmp((line + 1), "name", 4)))
		return (get_prog_name(*s_header, line + 5, len - 4));
	return (E_NONSE);
}

static char			asm_parser(char *line, t_instruct **s_instruct,
		t_header **s_header, t_op *s_op, t_lab_hdl *s_labels)
{
	int				ret;
	while (ft_isblank(*line))
		++line;
	if (!*line)
		return (1);
#if __DEBUG_ASM_PARSER__
		printf("\033[34mnew line: %s\n\033[0m", line);
#endif
	if (*line == '.')
		return (get_header_info(s_header, line));
	else if (*line == COMMENT_CHAR)
		return (get_comment());
	else
	{
		ret = get_instruct(&line, s_instruct, s_op, s_labels);
		return (ret);
	}
}

char				asm_parse(char **argv, t_header **s_header,
	t_instruct **s_instruct, t_lab_hdl *s_labels)
{
	int				fd;
	t_op			*s_op;
	char			*line;
	int				ret;
	int				line_nb;

	s_op = get_optab();
	ret = E_OPNIF;
	line_nb = 0;
	if (!(line_nb = 0) && !(fd = open(argv[1], O_RDONLY)))
		return (ret);
	while (((ret = get_next_line(fd, &line)) > 0) && ++line_nb
		&& (ret = asm_parser(line, s_instruct, s_header, s_op, s_labels)) > 0)
		free(line);
	if ((ret < 0) && (ret > SYS_ERR))
		parse_err(line_nb, ret, &line);
	return (ret);
}
