/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_instruct.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: k6 <klo.elka@gmail.com>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/23 04:49:20 by k6                #+#    #+#             */
/*   Updated: 2014/06/10 06:08:25 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <asm.h>

char				get_arg_type(char **p_line, t_op s_op, int n_arg)
{
	if (**p_line == REG_CHAR && M_ISSET((char)REG_CODE,
				(char)s_op.args_type[n_arg]) && ++*p_line)
		return (REG_CODE);
	else if (**p_line == DIRECT_CHAR && M_ISSET((char)DIR_CODE,
				(char)s_op.args_type[n_arg]) && ++*p_line)
		return (DIR_CODE);
	else if (M_ISSET((char)IND_CODE, (char)s_op.args_type[n_arg]))
		return (IND_CODE);
	return (-1);
}

char				get_instruct(char **line, t_instruct **s_instruct,
		t_op *s_op, t_lab_hdl *s_lab_hdl)
{
	char			*p_line;
	size_t			op_namlen;
	t_instruct		*p_instruct;

	p_instruct = add_instruct(s_instruct);
#if __DEBUG_ASM_PARSER__
	printf("\033[33m\tnew instruction:\n\033[0m");
#endif
	p_line = *line;
	op_namlen = 0;
	while (ft_isblank(*p_line))
		++p_line;
	while (ft_isalnum(*p_line) && ++op_namlen)
		++p_line;
	if (*p_line == LABEL_CHAR)
		add_label(line, &p_line, p_instruct, &op_namlen, s_lab_hdl);
	if (ft_isblank(*p_line))
		return (add_op(&p_line, p_instruct, op_namlen, s_op, s_lab_hdl));
	if (!*p_line)
		return (1);
	return (E_NONSE);
}
