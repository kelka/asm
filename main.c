/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: k6 <klo.elka@gmail.com>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/07 00:20:31 by k6                #+#    #+#             */
/*   Updated: 2014/06/11 14:18:28 by k6               ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <asm.h>

static void			asm_usage(void)
{
	ft_putstr("usage: ./asm champion.asm\n");
}

int					main(int argc, char **argv)
{
	t_instruct		*s_instructs;
	t_header		*s_header;
	int			status;
	t_lab_hdl		s_lab_hdl;

	status = 0;
	s_lab_hdl.labels = NULL;
	s_lab_hdl.ats = NULL;
	if (argc == 2)
	{
		s_instructs = NULL;
		s_header = NULL;
		if ((status = asm_parse(argv, &s_header, &s_instructs, &s_lab_hdl)) > -1)
			status = write_cor("test.cor", s_instructs, s_header, &s_lab_hdl);
		if (status <= SYS_ERR)
			sys_err(status);
		if (status <= 0)
			ft_putstr("Compilation failed, no output file created.\n");
		free_instruct(&s_instructs);
		free(s_header);
	}
	else
		asm_usage();
	return (status);
}
