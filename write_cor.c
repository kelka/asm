/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_cor.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: k6 <klo.elka@gmail.com>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/01 06:40:17 by k6                #+#    #+#             */
/*   Updated: 2014/06/11 15:06:08 by k6               ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <asm.h>


char					write_args(int fd, t_instruct *s_instruct, t_op s_op,
				unsigned int *prog_size, t_lab_hdl *s_lab_hdl)
{
		int					n_arg;
		int					type;
		int					len[MAX_ARGS_NUMBER];
		static t_at			*at = NULL;
		int					seeklen;

		n_arg = 0;
		if (!at)
				at = s_lab_hdl->ats;
		len[REG_CODE - 1] = REG_LEN;
		len[DIR_CODE - 1] = DIR_LEN;
		len[IND_CODE - 1] = IND_SIZE;
		printf("op: %s\n", s_op.name);
		while (n_arg < s_op.nb_args)
		{
				type = M_GET(s_instruct->args_type, n_arg);
				if ((type == DIR_CODE) && (*s_instruct->args[n_arg] == LABEL_CHAR))
				{
						at->offset = *prog_size;
						at = at->next;
						seeklen = s_op.has_idx ? IND_SIZE : DIR_LEN;
						*prog_size += seeklen;
				}
				else if ((type == DIR_CODE) && s_op.has_idx)
						EWRITE(fd, s_instruct->args[n_arg], IND_SIZE, prog_size);
				else
						EWRITE(fd, s_instruct->args[n_arg], len[type - 1], prog_size);
				++n_arg;
		}
		return (1);
}

void					 write_instruct(int fd, t_instruct *s_instruct,
				unsigned int *prog_size, t_lab_hdl *s_lab_hdl)
{
		static t_op			*s_op = NULL;
		t_labels			*label;

		if (!s_op)
				s_op = get_optab();
		if (s_instruct->label)
		{
				label = get_label(s_instruct->label, s_lab_hdl->labels);
				label->offset = *prog_size + 1;
		}
		WRITE(fd, &s_instruct->opcode, 1, prog_size);
		if (s_op[s_instruct->opcode - 1].nb_args > 1)
				WRITE(fd, &s_instruct->args_type, 1, prog_size);
		write_args(fd, s_instruct, s_op[s_instruct->opcode - 1], prog_size, s_lab_hdl);
}

char			write_labels(t_lab_hdl *s_lab_hdl, int fd, int begin)
{
		t_at		*at;
		int			offset;
		t_labels	*label;

		at = s_lab_hdl->ats;
		while (at)
		{
				printf("WESH\n");
				label = get_label(at->name, s_lab_hdl->labels);
				if (label)
				{
						printf("SEEK: %d len: %d\n", begin, (int)at->len);
						lseek(fd, SEEK_SET, at->offset + begin);
						offset = label->offset - at->offset;
						printf("offset: %x\n", offset);
						write(fd, swap_endian(&offset, at->len), (int)at->len);
						printf("get label %s at %x\n", label->name, offset);
				}
				at = at->next;
		}
		return (1);
}

char			write_cor(char *pathname, t_instruct *s_instruct,
				t_header *s_header, t_lab_hdl *s_lab_hdl)
{
		int			fd;
		int			begin;

		if ((fd = open(pathname, O_RDWR | O_CREAT, 0754)) < 0)
				return (E_READF);
		{
				write_head(fd, s_header);
				begin = s_header->prog_size;
				s_header->prog_size = 0;
				while (s_instruct)
				{
						if (s_instruct->opcode)
								write_instruct(fd, s_instruct, &s_header->prog_size, s_lab_hdl);
						s_instruct = s_instruct->next;
				}
				write_labels(s_lab_hdl, fd, begin);
				lseek(fd, sizeof(s_header->magic) + PROG_NAME_LENGTH, SEEK_SET);
				EWRITE(fd, &s_header->prog_size, sizeof(s_header->prog_size),
								&s_header->prog_size);
				printf("prog_size: %x\n", s_header->prog_size);
		}
		return (1);
}
